package br.ucsal.bes20191.junit;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.junit.Test;

import br.ucsal.bes20191.principal.Fatorial;

public class TesteObterNumero {

	InputStream stdin = System.in;
	
	@Test
	public void test1() {
		String string = "8\n";
		try {
		ByteArrayInputStream in = new ByteArrayInputStream(string.getBytes());
		System.setIn(in);
		int resultadoEsperado = 8;
		int resultadoAtual = Fatorial.obterNumero();
		assertEquals(resultadoEsperado, resultadoAtual);
		} finally {
			System.setIn(stdin);
		}
	}
	
	@Test
	public void test2() {
		String string = "9\n";
		try {
		ByteArrayInputStream in = new ByteArrayInputStream(string.getBytes());
		System.setIn(in);
		int resultadoEsperado = 9;
		int resultadoAtual = Fatorial.obterNumero();
		assertEquals(resultadoEsperado, resultadoAtual);
		} finally {
			System.setIn(stdin);
		}
	}
	
	@Test
	public void test3() {
		String string = "10\n";
		try {
		ByteArrayInputStream in = new ByteArrayInputStream(string.getBytes());
		System.setIn(in);
		int resultadoEsperado = 10;
		int resultadoAtual = Fatorial.obterNumero();
		assertEquals(resultadoEsperado, resultadoAtual);
		} finally {
			System.setIn(stdin);
		}
	}

}
