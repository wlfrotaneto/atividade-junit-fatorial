package br.ucsal.bes20191.junit;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import br.ucsal.bes20191.principal.Fatorial;

public class TesteCalcularFatorial {

	@ParameterizedTest
	@ValueSource(ints = { 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 })
	public void testCalcularFatorial(int numero) {
		double resultadoEsperado = 0;
		int passarvalor = numero;
		double resultadoAtual = Fatorial.calcularFatorial(passarvalor);
		double fat = 1;
		for( int i = 2; i <= numero; i++ ) { 
		     fat *= i;
		}
		resultadoEsperado = fat;
		assertEquals(resultadoEsperado, resultadoAtual);
	}
}
