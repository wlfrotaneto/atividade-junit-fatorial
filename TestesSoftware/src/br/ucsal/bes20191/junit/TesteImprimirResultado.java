package br.ucsal.bes20191.junit;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import br.ucsal.bes20191.principal.Fatorial;

public class TesteImprimirResultado {

	@ParameterizedTest
	@ValueSource(strings = { "8", "9", "10" })
	public void testImprimirResultado(String string) {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		System.setOut(new PrintStream(out));
		Fatorial.imprimirResultado(Integer.parseInt(string), Fatorial.calcularFatorial(Integer.parseInt(string)));
		String resultadoAtual = out.toString();
		String resultadoEsperado = "O fatorial de " + Integer.parseInt(string) + " �: "
				+ Fatorial.calcularFatorial(Integer.parseInt(string));
		assertEquals(resultadoEsperado, resultadoAtual);
	}
}
