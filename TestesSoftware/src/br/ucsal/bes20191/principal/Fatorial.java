package br.ucsal.bes20191.principal;
 
import java.util.Scanner;
 
public class Fatorial {
 
    private static Scanner scanner = new Scanner(System.in);
 
    public static void main(String[] args) {
        Integer numero = obterNumero();
        imprimirResultado(numero, calcularFatorial(numero));
    }
 
    public static Integer obterNumero() {
        Integer numero;
        do {
            System.out.println("Escreva um n�mero entre 0 e 100: ");
            numero = scanner.nextInt();
            if (numero < 0 || numero > 100)
                System.out.println("N�mero inv�lido!");
        } while (numero < 0 || numero > 100);
        return numero;
    }
   
    public static Double calcularFatorial(Integer numero){
        if (numero <= 1) {
            return 1d;
        }
        return calcularFatorial(numero - 1) * numero;
    }
 
    public static void imprimirResultado(Integer numero, Double fatorial){
        System.out.print("O fatorial de " + numero + " �: " + fatorial);
    }
}
